/*
 * Copyright 2008 Jim Driscoll
 * Copyright 2013 Blue Lotus Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.jsf;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author Jim Driscoll
 * @author John Yeary
 * @version 2.0
 */
@ManagedBean
@ViewScoped
public class ListHolder implements Serializable {

    private static final long serialVersionUID = -933704272568697599L;
    private final Map<String, String> items1 = new LinkedHashMap<String, String>();
    private final Map<String, String> items2 = new LinkedHashMap<String, String>();
    private String[] list1 = null;
    private String[] list2 = null;

    public ListHolder() {
        resetLists();
    }

    public void move1to2(ActionEvent event) {
        if (list1 != null && list1.length > 0) {
            for (String item : list1) {
                items2.put(item, items1.remove(item));
            }
        }
    }

    public void move2to1(ActionEvent event) {
        if (list2 != null && list2.length > 0) {
            for (String item : list2) {
                items1.put(item, items2.remove(item));
            }
        }
    }

    public Map<String, String> getItems1() {
        return items1;
    }

    public Map<String, String> getItems2() {
        return items2;
    }

    public String[] getList1() {
        return list1;
    }

    public void setList1(String[] list1) {
        this.list1 = list1;
    }

    public String[] getList2() {
        return list2;
    }

    public void setList2(String[] list2) {
        this.list2 = list2;
    }

    public void resetLists() {

        items1.clear();
        items2.clear();

        items1.put("one", "one");
        items1.put("two", "two");
        items1.put("three", "three");
        items1.put("four", "four");

        items2.put("five", "five");
        items2.put("six", "six");
        items2.put("seven", "seven");
        items2.put("eight", "eight");
    }
}
